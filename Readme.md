CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

MSG91 SMS provides a block to send SMS form Drupal website.
It provides easy integration of msg91 to be used in India.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/msg91

 * To submit bug reports and feature suggestions, or to track
   changes: https://www.drupal.org/project/issues/msg91


REQUIREMENTS
------------

This module doesn't require any module outside of Drupal core.


INSTALLATION
------------

 * Install this module as you would normally install a contributed
   Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

   1. Create an account on MSG91 and Get Authentication Key
      (https://msg91.com/)

   2. Install and enable this module.

   3. Go to /admin/config/msg91/settings and enter values of Authentication
      Key, Sender ID, Route and Country Code in configuration settings.


MAINTAINERS
-----------

 * Gaurav Kapoor https://www.drupal.org/u/gauravkapoor
 * Vidhatanad https://www.drupal.org/u/vidhatanand

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
 * Axelerant - https://www.drupal.org/axelerant
